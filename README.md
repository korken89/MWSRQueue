# A C++ library for a generic Multiple Writer, Single Reader queue

## Contributors

* Emil Fresk

---

## License

Licensed under the LGPL-v3 license, see LICENSE file for details.

---

