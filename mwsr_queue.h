/****************************************************************************
*
* Copyright (C) 2015 Emil Fresk.
* All rights reserved.
*
* This file is part of the SerialPipe library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#ifndef _MWSR_QUEUE_H
#define _MWSR_QUEUE_H

/* Data includes */
#include <vector>
#include <queue>
#include <algorithm>

/* Threading includes */
#include <thread>
#include <mutex>
#include <condition_variable>

/**
 * @brief  A simple Multiple Writer, Single Reader queue implementation that is
 *         thread safe. Written for the C++14 standard.
 */
template <class T>
class MWSRQueue
{
    private:
        std::deque<T> _queue;
        std::mutex _qlock;
    public:
        std::condition_variable data_available;

        MWSRQueue()
        {
        }

        void enqueue(T item)
        {
            std::lock_guard<std::mutex> locker(_qlock);

            _queue.emplace_back(item);

            data_available.notify_one();
        }

        void enqueueMultiple(std::vector<T> items)
        {
            std::lock_guard<std::mutex> locker(_qlock);

            /* Enqueue all items in the vector */
            for (auto &item: items)
            {
                _queue.emplace_back(item);
            }

            data_available.notify_one();
        }

        T dequeue(void)
        {
            std::lock_guard<std::mutex> locker(_qlock);

            /* Read the first element and return it */
            T tmp = _queue.front();
            _queue.pop_front();

            return tmp;
        }

        std::vector<T> dequeueAll(void)
        {
            std::lock_guard<std::mutex> locker(_qlock);

            /* Allocate the memory for the queue */
            std::vector<T> v_queue;
            v_queue.reserve(_queue.size());

            /* Read out the data from the queue, a little ugly for now. */
            std::copy(_queue.begin(), _queue.end(), std::back_inserter(v_queue));

            /* Empty the queue */
            _queue.clear();

            /* Return by value */
            return v_queue;
        }

        int size(void)
        {
            std::lock_guard<std::mutex> locker(_qlock);

            return _queue.size();
        }
};

#endif
